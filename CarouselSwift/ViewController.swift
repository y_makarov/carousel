//
//  ViewController.swift
//  CarouselSwift
//
//  Created by Yevhen Makarov on 15.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, iCarouselDelegate, iCarouselDataSource{
    

    @IBOutlet var carouselView: iCarousel!
    var numbers = [UIImage]()
    override func awakeFromNib() {
        super.awakeFromNib()
         numbers = [ #imageLiteral(resourceName: "event-536-1470870017-ny-power-women-large"), #imageLiteral(resourceName: "images-4"), #imageLiteral(resourceName: "images"), #imageLiteral(resourceName: "images-3"), #imageLiteral(resourceName: "original"), #imageLiteral(resourceName: "2manhattanborostill__large")]
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.type = .invertedCylinder
    }

    func numberOfItems(in carousel: iCarousel) -> Int {
        return numbers.count
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        myView.backgroundColor = UIColor.white
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
       // button.setTitle("\(numbers[index])", for: .normal)
        button.setImage(numbers[index], for: .normal)
        myView.addSubview(button)
        return myView
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing {
            return value * 1.1
        }
        return value
    }

}

